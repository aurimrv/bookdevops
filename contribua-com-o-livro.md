# Contribua com o Livro

Nós aceitamos todo tipo de contribuição e apoio na redação e melhoria do conteúdo deste livro. Fique à vontade para mandar críticas, sugestões e melhorias via [Issue](https://gitlab.com/aurimrv/bookdevops/-/issues) no nosso repositório do [GitLab](https://gitlab.com/aurimrv/bookdevops). Outra opção é encaminhar sugestões de melhoria para o e-mail [auri@ufscar.br](mailto:auri@ufscar.br) com o **Assunto: Livro DevOps**.
