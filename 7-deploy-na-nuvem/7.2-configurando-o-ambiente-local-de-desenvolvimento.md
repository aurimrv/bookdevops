# 7.2 Configurando o ambiente local de desenvolvimento

A configuração do ambiente visa permitir ao desenvolvedor executar localmente a aplicação em um cluster na sua própria máquina, bem como possibilitar que seja realizado o _deploy_ em uma nuvem pública utilizando os mesmos arquivos para a definição dos recursos.&#x20;

Com isso, obtém-se um ambiente local que é muito mais próximo ao ambiente de produção, diminuindo as chances de ocorrerem erros devido às diferenças entre os ambientes, que poderiam ocorrer, por exemplo, ao utilizar o Docker Compose localmente e o Kubernetes no ambiente de produção.

Serão utilizadas diversas ferramentas como kubectl, k0s, Skaffold e Helm e Terraform, as quais terão suas funcionalidades apresentadas e discutidas. A seguir é apresentado o modo de configurar o ambiente local, utilizando as plataformas Linux e Windows.

## Linux Ubuntu

### Instalação do k0s

A primeira ferramenta que vamos instalar é o [k0s](https://k0sproject.io/) , o qual permite que criemos um _cluster_ localmente na nossa máquina para testarmos nossa aplicação. Ele também será utilizado para criar o _cluster_ que será utilizado no ambiente de produção. Ele pode ser instalado com o seguinte comando:

```
curl -sSLf https://get.k0s.sh | sudo sh
```

Podemos então instalar um serviço que permitirá a criação de um _cluster_ (com uma única máquina) com o seguinte comando (será necessário digitar sua senha para entrar no modo de super usuário):

```
sudo k0s install controller --single
```

Uma vez instalado o serviço, devemos executá-lo (após esse passo, o serviço executará automaticamente sempre que a máquina for reiniciada):

```
sudo k0s start
```

Após um ou dois minutos, o cluster estará funcional. Para verificar o status do serviço, pode-se utilizar o seguinte comando:

```
sudo k0s status
```

A resposta deverá ser semelhante a essa:

```
Version: v0.11.0
Process ID: 436
Parent Process ID: 1
Role: controller+worker
Init System: linux-systemd
```

### Instalação do kubectl

A ferramenta de linha de comando kubectl permite a execução de comandos para interação com o cluster criado. Essa ferramenta é bem interessante, pois permite, por exemplo, a utilização de seus comandos em scripts de automatização. É possível utilizar a versão que vem disponível com o próprio k0s, do seguinte modo:

```
sudo k0s kubectl COMANDO_DO_KUBECTL
```

Onde COMANDO\_DO_\__KUBECTL deve ser substituído por algum comando específico do kubect. Por exemplo, podemos ver os nós do cluster desse modo:

```
sudo k0s kubectl get nodes
```

No entanto, também é possível instalar uma versão arbitrária do kubectl usando um repositório. Esse modo é recomendável, pois assim é possível ter um controle maior sobre sua versão e suas atualizações.

O primeiro passo é atualizar o índice de pacotes do Ubuntu e instalar as dependências necessárias:

```
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
```

Na sequencia, deve ser baixada a chave GPG do repositório:

```
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
```

Deve-se adicionar o repositório ao índice de pacotes do Ubuntu:

```
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
```

Atualizar o índice e por fim instalar o kubect:

```
sudo apt-get update
sudo apt-get install -y kubectl
```

Desse modo, o kubectl poderá ser executado assim:

```
sudo kubectl COMANDO_DO_KUBECTL
```

Porém será necessário executar os passos da próxima seção.

### Configurando o kubectl para acessar o cluster do k0s

Se você instalou o kubectl em vez de usar o kubectl do k0s, necessitará executar os seguintes passos. É necessário criar um arquivo de configuração do kubectl, chamado config. O k0s tem u comando que cria essa configuração, e deve ser utilizado do seguinte modo:

`sudo k0s kubeconfig create --groups "system:masters" NOME_USUARIO > k0s.config`

Devemos substituir o NOME\_USUARIO por um nome arbitrário, que será o nome do usuário que vamos utilizar para a acessar o cluster, por meio do kubectl.

Na sequencia, vamos criar o usuário no cluster, com a permissão de administrador, do seguinte modo (não se esqueça de substituir novamente o NOME\_DO_\__USUARIO pelo nome definido no comando anterior) e jogar a saída para o arquivo k0s.config:

```
sudo k0s kubectl create clusterrolebinding NOME_USUARIO-binding --clusterrole=admin --user=NOME_USUARIO > k0s.config
```

Será gerado um arquivo semelhante a esse:

```
apiVersion: v1
clusters:
- cluster:
    server: https://192.168.0.15:6443
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURBRENDQWVpZ0F3SUJBZ0lVVEdYWWsvcGF6M01VY29FL0dBOGUyZjJ5VVVNd0RRWUpLb1pJaHZjTkFRRUwKQlFBd0....
  name: k0s
contexts:
- context:
    cluster: k0s
    user: NOME_USUARIO
  name: k0s
current-context: k0s
kind: Config
preferences: {}
users:
- name: NOME_USUARIO
  user:
    client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURWVENDQWoyZ0F3SUJBZ0lVTWdCMEtHcTk1M0ZjRjNXTEtDWU1vWEhZd2NVd0RRWUpLb1pJaHZjTkFRRUwKQlFBd0....
    client-key-data: LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFcEFJQkFBS0NBUUVBMXlyUEpndFRSdmJFS3orNUh3SFV5U2ZoRTU3TjN0d0VLdmVtcjhDTU9vbGM0UWVWCn....
```

Devemos agora mover esse arquivo para o diretório padrão de configuração do kubectl:

`mv k0s.config $HOME/.kube/config`

No arquivo gerado, na linha 12, temos a configuração `current-context: k0s`, que informa ao kubectl qual o contexto deve ser usado. Essa informação é interessante, pois podemos definir no arquivo diferentes contextos, e permitir a conexão a diferentes clusters Kubernetes. Também é possível definir o contexto atual usando a linha de comando:

`kubectl config use-context k0s`

Para verificar qual o contexto está sendo utilizado atualmente, é possível usar o seguinte comando:

`kubectl config current-context`

### Instalação do Registry do Docker

Vamos precisar configurar um registry de imagens Docker. Nele, as imagens geradas pela nossa aplicação será armazenadas, e posteriormente acessadas pelo Kubernetes para a criação dos nossos containers.

Um único comando para o Docker inicializa o registry, na porta 5005:&#x20;

`docker run -d -p 5005:5000 --restart=always --name registry registry:2`

A seguir, devemos alterar o nosso arquivo /etc/hosts, para criar um alias para o registry. Desse modo, vamos poder acessá-lo pela url `http://registry-local:5005`. Para isso, devemos editar o arquivo /etc/hosts e buscar a linha que contenha esse conteúdo:

&#x20;`127.0.1.1 nome-da-maquina`

Ela é uma das primeiras linhas desse arquivo. Vamos alterá-la para acrescentar o alias do registry:

&#x20;`127.0.1.1 nome-da-maquina registry-local`

Basta salvar o arquivo, para terminar a definição do alias. Para testar se está tudo correto, podemos utilizar o curl:

`curl registry-local:5005/v2/`

A resposta deve ser `{}`.

Por esse registry e o cluster rodarem localmente na nossa máquina, não precisamos definir um acesso seguro a ele, utilizando certificados SSL. No entanto, teremos que realizar algumas configurações no k0s para que o cluster Kubernetes possa acessar esse registry de maneira insegura. Para isso, vamos gerar um arquivo de configuração utilizado pelo k0s:

`sudo containerd config default > containerd.toml`

Devemos editar o arquivo containerd.toml gerado, e alterar as seguintes linhas:

`root = "/var/lib/containerd" state = "/run/containerd"`

Para que fiquem assim:

`root = "/var/lib/k0s/containerd" state = "/var/lib/k0s/run/containerd"`

Já a linha:

```
address = "/run/containerd/containerd.sock"
```

Deverá ficar assim:

```
address = "/var/lib/k0s/run/containerd.sock"
```

Por último, logo abaixo da linha:

```
endpoint = ["https://registry-1.docker.io"]
```

Vamos acrescentar o seguinte trecho:

```
[plugins."io.containerd.grpc.v1.cri".registry.mirrors."registry-local:5005"]
          endpoint = ["http://registry-local:5005"]
        [plugins."io.containerd.grpc.v1.cri".registry.configs]
          [plugins."io.containerd.grpc.v1.cri".registry.configs."registry-local:5005".tls]
            insecure_skip_verify = true
```

De modo que essa parte do arquivo fique assim:

```
    [plugins."io.containerd.grpc.v1.cri".registry]
      [plugins."io.containerd.grpc.v1.cri".registry.mirrors]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]
          endpoint = ["https://registry-1.docker.io"]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."registry-local:5005"]
          endpoint = ["http://registry-local:5005"]
        [plugins."io.containerd.grpc.v1.cri".registry.configs]
          [plugins."io.containerd.grpc.v1.cri".registry.configs."registry-local:5005".tls]
            insecure_skip_verify = true
```

Lembre-se de manter a identação correta para que não haja problemas. Após essas alterações, vamos copiar esse arquivo para o diretório que o k0s usa para ler suas configurações:

```
sudo mkdir /etc/k0s/
sudo mv containerd.toml /etc/k0s/
```

Agora é necessário reiniciar o k0s com os dois comandos abaixo para que a alteração entre em vigor:

```
sudo service k0scontroller stop
sudo service k0scontroller start
```

Para checar se não houve nenhum problema, execute o comando a seguir:

```
sudo service k0scontroller status
```

A saída deverá indicar que o serviço está ativo:

```
● k0scontroller.service - k0s - Zero Friction Kubernetes
     Loaded: loaded (/etc/systemd/system/k0scontroller.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2021-06-09 09:35:50 -03; 2h 25min ago
```

Agora que já estão corretos o _cluster_, o registry, e a comunicação entre eles, vamos instalar o Skaffold.

### Instalação do Skaffold

O [Skaffold](https://skaffold.dev/) é uma ferramenta que automatiza os processos de:

* Geração de imagem
* Envio da imagem para um _registry_
* _Deploy_ da aplicação no cluster Kubernetes

Com isso, o desenvolvedor consegue ficar focado somente no desenvolvimento da aplicação. Utilizaremos ele para realizar o _deploy_ da aplicação tanto no cluster local, quanto no cluster do ambiente de produção. A sua instalação é bem simples e pode ser realizada com o seguinte comando, o qual instala a versão mais nova:

`curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64 && sudo install skaffold /usr/local/bin/`

Para verificar a versão instalada, é possível utilizar o comando `skaffold version`, que deverá retornar algo semelhante a:

`v1.25.0`

Vamos agora definir que o repositório padrão de imagens, ou seja, o registry que será utilizado para enviar e obter imagens Docker, será o registry que instalamos:

```
skaffold config set --global default-repo registry-local:5005
skaffold config set --global insecure-registries registry-local:5005
```

Para confirmar que as configurações foram aplicadas com sucesso, execute o seguinte comando:

```
skaffold config list
```

A saída dele deve ser semelhante a:

```
skaffold config: 
global:
  default-repo: registry-local:5005
  insecure-registries:
  - registry-local:5005
  survey:
    last-prompted: "2021-06-07T19:22:17-03:00"
  collect-metrics: true
  update:
    last-prompted: "2021-06-08T17:39:15-03:00"
kubeContexts:
- kube-context: k0s
```

Uma vez configurado o Skaffold, podemos finalmente utilizá-lo.

## Instalação do Helm

O [Helm](https://helm.sh/) é uma ferramenta que gerencia pacotes do Kubernetes, e vai permitir que criemos uma pacote, denominado _chart_ para distribuir, instalar e atualizar nossa aplicação em um cluster Kubernetes.

O Skaffold consegue utilizar o Helm para instalar a nossa aplicação no cluster, e com a utilização do Helm podemos ter diferentes configurações de acordo com o ambiente no qual vamos realizar o deploy da aplicação. Desse modo, por exemplo, podemos controlar o endereço no qual a aplicação vai estar disponível, a quantidade de memória e processamento que ela vai poder utilizar, entre outras configurações que podem variar de um ambiente para outro.

A instalação do Helm é bem simples no Linux, bastando executar os comandos a seguir:

```shell
$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
$ chmod 700 get_helm.sh
$ ./get_helm.sh
```

Para verificar a versão instalada, execute:

```
helm version
```

## Instalação do Terraform

O [Terraform](https://www.terraform.io/) é uma aplicação que permite codificar a infraestrutura necessária para executar a aplicação como código, o qual pode ser armazenado e versionado em repositórios de código, tais como GitHub e GitLab.

Ao transformar a infraestrutura em código versionado, é possível ter um histórico atualizado das alterações promovidas na infraestrutura, e eventualmente reverter de modo simplificado as alterações realizadas, caso ocorra algum problema.

Para instalar o Terraform é necessário adidiconar a chave GPG do repositório:

```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
```

Na sequencia deve ser adicionado o repositório do Terraform:

```
sudo sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
```

E por último atualizar o repositório e instalar a ferramenta:

```
sudo apt-get update && sudo apt-get install terraform
```

Para conferir a versão instalada use o seguinte comando:

```
terraform -v
```

Após a configuração do ambiente local de desenvolvimento, vamos entender os recursos do Kubernetes que utilizaremos para realizar o _deploy_ da aplicação na próxima seção.
